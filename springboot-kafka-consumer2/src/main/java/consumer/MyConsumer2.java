package consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class MyConsumer2 {

    @KafkaListener(topics = "topic-spring-1")
    public void onMessage(ConsumerRecord<Integer, String> record) {
        System.out.println("Consumer2-消费者收到的消息：" + record.value());
    }

}
